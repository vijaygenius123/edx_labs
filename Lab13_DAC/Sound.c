// Sound.c
// Runs on LM4F120 or TM4C123, 
// edX lab 13 
// Use the SysTick timer to request interrupts at a particular period.
// Daniel Valvano, Jonathan Valvano
// March 13, 2014
// This routine calls the 4-bit DAC

#include "Sound.h"
#include "DAC.h"
#include "..//tm4c123gh6pm.h"

// DAC index
// ---------
volatile unsigned long DAC_IDX   = 0; // Index varies from 0 to 15
volatile unsigned long SOUND_OFF = 1; // True, False

// 4-bit 32-element wave tables
// -----------------------------
const unsigned short silence_wave[32] = {0, 0, 0, 0, 0, 0, 0, 0,
																				 0, 0, 0, 0, 0, 0, 0, 0,
																				 0, 0, 0, 0, 0, 0, 0, 0,
																				 0, 0, 0, 0, 0, 0, 0, 0};

const unsigned short sin_wave_16[16] = {8,11,13,14,15,14,
																		 13,11,8,5,3,
																		 2,1,2,4,7};
																				 

const unsigned short sin_wave[32] = {8,9,11,12,13,14,14,15,15,15,14,
																		 14,13,12,11,9,8,7,5,4,3,2,
																		 2,1,1,1,2,2,3,4,5,7};

const unsigned char Trumpet[32]   = {10,11,11,12,10,8,3,1,8,15,15,
																		 11,10,10,11,10,10,10,10,10,10,10,
																		 10,11,11,11,11,11,11,10,10,10};  

const unsigned char Horn[64]      = {7,8,8,8,8,9,10,12,15,15,15,
																		 13,10,7,4,3,3,3,3,3,3,3,
																		 4,4,4,4,5,6,7,8,8,9,
																		 9,10,11,11,12,13,13,12,12,13,
																		 14,12,11,9,8,6,3,2,1,1,
																		 0,1,1,1,2,2,3,4,4,6,7,7}; 

																		 
unsigned char *GLOBAL_WAVE;


// **************Sound_Init*********************
// Initialize Systick periodic interrupts
// Also calls DAC_Init() to initialize DAC
// Input: none
// Output: none
void Sound_Init(void)
{
	GLOBAL_WAVE = (unsigned char *)sin_wave;
	DAC_IDX 	  = 0;
	SOUND_OFF   = 1;
	DAC_Init ();
	
		// Systick init
	NVIC_ST_CTRL_R    = 0;                                         // disable SysTick during setup
  NVIC_ST_CURRENT_R = 0;                                         // any write to current clears it
	NVIC_ST_RELOAD_R  = 80000;                                     // Init 100 Hz
	NVIC_SYS_PRI3_R 	= (NVIC_SYS_PRI3_R&0x00FFFFFF) | 0x20000000; // priority 1               
  NVIC_ST_CTRL_R 		= 0x00000007;  		                           // enable with core clock and interrupts
  
}

// **************Sound_Tone*********************
// Change Systick periodic interrupts to start sound output
// Input: interrupt period
//           Units of period are 12.5ns
//           Maximum is 2^24-1
//           Minimum is determined by length of ISR
// Output: none
void Sound_Tone(unsigned long period)
{
	// Systick init
	
	 if (NVIC_ST_RELOAD_R == period) return;
	
	 NVIC_ST_CTRL_R    = 0;                                         // disable SysTick during setup
	 SOUND_OFF         = 0;
	 DAC_IDX 					 = 0;
	 NVIC_ST_CURRENT_R = 0;                                         // any write to current clears it
	 NVIC_ST_RELOAD_R  = period ;
   NVIC_SYS_PRI3_R 	 = NVIC_SYS_PRI3_R & 0x00FFFFFF; 						 // priority 0               
   NVIC_ST_CTRL_R 	 = 0x00000007;  		                           // enable with core clock and interrupts
}


// **************Sound_Off*********************
// stop outputing to DAC
// Output: none
void Sound_Off(void)
{
  SOUND_OFF = 1;	
	Sound_Tone (0);
}


// Interrupt service routine
// Executed every 12.5ns*(period)
void SysTick_Handler(void)
{
		if (SOUND_OFF)  
		{
			DAC_Out (0);
		}
	  else 
		{
			DAC_IDX = (DAC_IDX + 1) & 0x1F ;
			DAC_Out (sin_wave[DAC_IDX]);
		}
	  
}
