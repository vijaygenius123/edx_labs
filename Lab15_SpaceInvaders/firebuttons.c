// Piano.c
// Runs on LM4F120 or TM4C123, 
// edX lab 13 
// There are four keys in the piano
// Daniel Valvano
// September 30, 2013

// Port E bits 3-0 have 4 piano keys

#include "firebuttons.h"
#include "..//tm4c123gh6pm.h"

// Const declarations
// ---------------------

#define KEY_FIRE1  (* ((volatile unsigned long *) 0x40024004))  // PE0 in
#define KEY_FIRE2  (* ((volatile unsigned long *) 0x40024008))  // PE1 in
#define LED1       (* ((volatile unsigned long *) 0x40024020))  // PE3 out
#define LED2       (* ((volatile unsigned long *) 0x40024040))  // PE4 out
	
// Forward declarations
// ---------------------
void Delay1ms (unsigned long msec);

// Helper functions
void Keys_Init (void);
void Lights_Init (void);


// **************Piano_Init*********************
// Initialize piano key inputs
// Input: none
// Output: none
// Summary: Initialize Piano key inputs in port A
void Fire_Init(void)
{ 
	Keys_Init ();
	Lights_Init ();
}

void Keys_Init (void)
{
	unsigned long volatile initDelay;
	
	// Port E clock
	SYSCTL_RCGC2_R     |= 0x10;           
  initDelay           = SYSCTL_RCGC2_R;       // wait 3-5 bus cycles
	
	// Port E
  GPIO_PORTE_DIR_R   &= ~0x03;        // PE0, PE1 input 
  GPIO_PORTE_AFSEL_R &= ~0x03;      // not alternative
  GPIO_PORTE_AMSEL_R &= ~0x03;      // no analog
  GPIO_PORTE_PCTL_R  &= ~0x000000FF; // bits for PE0
  GPIO_PORTE_DEN_R   |= 0x03;         // enable PE0
	GPIO_PORTE_PUR_R   &= ~0x03;        // Pull up disable, we're going to connect it outside

}

void Lights_Init (void)
{
	unsigned long volatile initDelay;
	
	// Port B clock
	SYSCTL_RCGC2_R     |= 0x02;           
  initDelay           = SYSCTL_RCGC2_R;       // wait 3-5 bus cycles

	// Port B
  GPIO_PORTB_DIR_R 	 |= 0x30;           // PB4, PB5 output
  GPIO_PORTB_AFSEL_R &= ~0x30;          // not alternative
  GPIO_PORTB_AMSEL_R &= ~0x30;          // no analog
  GPIO_PORTB_PCTL_R  &= ~0x00FF0000;    // bits for PB4, PB5
  GPIO_PORTB_DEN_R   |= 0x30;           // enable PB4, PB5
	GPIO_PORTB_PUR_R   &= ~0x30;          // Pull up disable, we're going to connect it outside
	
}

// **************Fire in*********************
// Input from fire key inputs
// Input: none 
// Output: 0 to 15 depending on keys
// 0x01 is key 0 pressed, 0x02 is key 1 pressed,
// 0x04 is key 2 pressed, 0x08 is key 3 pressed
unsigned long Fire_In(void)
{
	unsigned long key_pressed = NONE;
	
	if (KEY_FIRE1)   key_pressed 						= FIRE1;
	if (KEY_FIRE2)   key_pressed 					  = FIRE2;
	if (KEY_FIRE1 && KEY_FIRE2) key_pressed = FIRE12;
	
	return key_pressed;
}


void FireOff_LEDS (void)
{
	GPIO_PORTB_DATA_R &= ~0x30;
}
void Fire_LED (void)
{
	GPIO_PORTB_DATA_R |= 0x10;
}

void Special_FireLED (void)
{
	GPIO_PORTB_DATA_R |= 0x20;
}

// **************Fire_Testing *********************
// output  Fire_Testing
int Fire_Testing (void)
{
		switch (Fire_In ())
		{
			case FIRE1:  Fire_LED ();
                   break;
			case FIRE2:  Special_FireLED ();
								   break;
			case FIRE12: Fire_LED ();
							     Special_FireLED ();
								   break;
			default: 		 FireOff_LEDS ();
								 
		}
		return 0;
}

