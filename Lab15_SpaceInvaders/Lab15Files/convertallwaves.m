inputDir = 'C:\Keil\Labware\Lab15_SpaceInvaders\Lab15Files\Sounds\';
files = {'explosion', ...    
         'fastinvader1', ... 
         'fastinvader2',  ...
         'fastinvader3',  ...
         'fastinvader4',  ...
         'invaderkilled', ...
         'shoot',         ...
         'ufo_highpitch', ...
         'ufo_lowpitch'};
     
inputFiles  = arrayfun (@(x) strcat (inputDir, x, '.wav'), files);
outputFiles = arrayfun (@(x) strcat (inputDir, x, '.h'), files);

targetFs (1:length(files)) = 11025;
breakline (1:length (files)) = 12;

for i=1: length (files)
    wavdownsample (char (inputFiles(i)), ...
                   char (targetFs (i)), ...
                   char (outputFiles(i)), ...
                   char (files(i)), ...
                   breakline(i));
end