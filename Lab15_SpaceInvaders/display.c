#ifndef __DISPLAY__
#define __DISPLAY__

#include "display.h"
#include "graphics.h"
#include "Nokia5110.h"

extern void Delay100ms(unsigned long count); // time delay in 0.1 seconds

void Screen_Init ()
{
	Nokia5110_Init();
	Nokia5110_Clear();
  Nokia5110_ClearBuffer();
	Nokia5110_DisplayBuffer();      // draw buffer
}

void Screen_Menu (void)
{
	Nokia5110_Clear();
	Nokia5110_ClearBuffer();
	Nokia5110_SetCursor (1, 1);
  Nokia5110_OutString (("Press Fire "));
	Nokia5110_SetCursor (2, 3);
	Nokia5110_OutString (("to Start "));
	Delay100ms(1);  // delay 5 sec at 50 MHz
}

void Screen_ShowDead (void)
{
	Nokia5110_Clear();
	Nokia5110_ClearBuffer();
	Nokia5110_SetCursor (1, 0);
  Nokia5110_OutString (("We are all"));
	Nokia5110_SetCursor (5, 2);
  Nokia5110_OutString (("..."));
	Nokia5110_SetCursor (3, 4);
	Nokia5110_OutString (("DEAD!!!"));
	Nokia5110_SetCursor (4, 5);
	Nokia5110_OutString ((":-("));
  Delay100ms(100);  // delay 5 sec at 50 MHz
}


void Screen_ShowLives   (unsigned long livesleft)
{
	char sLives [2];
	sLives[0] = livesleft + 48;
	sLives[1] = '\0';
	
	Nokia5110_Clear();
	Nokia5110_ClearBuffer();
	Nokia5110_SetCursor (11, 2);
	Nokia5110_OutString (sLives);
	Nokia5110_SetCursor (0, 2);
	Nokia5110_OutString (("Lives left:"));
	Delay100ms(10);  // delay 5 sec at 50 MHz
}

void Screen_Entry (void)
{
	Nokia5110_SetCursor (0, 0);
	Nokia5110_OutString (("Once upon "));
	Nokia5110_SetCursor (0, 1);
	Nokia5110_OutString (("a time ... "));
  Nokia5110_SetCursor (0, 2);
	Nokia5110_OutString (("I'd a cool "));
	Nokia5110_SetCursor (5, 3);
	Nokia5110_OutString (("4k"));
	Nokia5110_SetCursor (0, 4);
	Nokia5110_OutString (("Entry Screen"));
	Nokia5110_SetCursor (0, 5);
	Nokia5110_OutString (("SpaceInvader"));
	
  // SpriteEntryScreen (); // 4K
	// Nokia5110_DisplayBuffer();     // draw buffer
	Delay100ms(30);              // delay 5 sec at 50 MHz
}

void Screen_GameOver ()
{
	Nokia5110_Clear();
  Nokia5110_SetCursor(1, 1);
  Nokia5110_OutString("GAME OVER");
  Nokia5110_SetCursor(1, 2);
  Nokia5110_OutString("Nice try,");
  Nokia5110_SetCursor(1, 3);
  Nokia5110_OutString(":-P");
  Nokia5110_SetCursor(2, 4);
  Nokia5110_OutUDec(1234);
}

void Screen_Refresh (unsigned long count)
{
	Nokia5110_DisplayBuffer();      					// draw buffer
	if (count) Delay100ms(count);             // delay 5 sec at 50 MHz
  Nokia5110_Clear();
  Nokia5110_ClearBuffer();
}

#endif // __DISPLAY__

