#ifndef _FX_SOUNDS_H_
#define _FX_SOUNDS_H_

#define SILENCE  0x0  
#define DO       0x01 // C @ 523.251 Hz
#define RE       0x02 // D @ 587.330 Hz
#define MI       0x04 // E @ 659.255 Hz
#define SOL      0x08 // G @ 783.991 Hz

// Const frecuency notes
// -----------------------------

#define DO_PERIOD  4778  // 80MHz / (523.251Hz * len (wave_table))
#define RE_PERIOD  4256  // 80MHz / (587.330Hz * len (wave_table))
#define MI_PERIOD  3792  // 80MHz / (659.255Hz * len (wave_table))
#define SOL_PERIOD 6378  // 80Mzh / (392       * len (wave_table))


#endif // _FX_SOUNDS_H_
