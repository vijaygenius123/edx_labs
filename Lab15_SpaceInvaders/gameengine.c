#include "gameengine.h"
#include "display.h"
#include "graphics.h"
#include "sound.h"
#include "firebuttons.h"
#include "ADC.h"

typedef enum {START, RUNNING, SHOWLIVES, DEAD, MENU} GameStatus;

// Some externs ...
// ----------------
extern unsigned long Refresh;
extern void Delay100ms (unsigned long count); // time delay in 0.1 seconds
extern void Delay1ms   (unsigned long count); // time delay in 0.1 seconds

// Game Engine Globals
// -------------
unsigned long TimerCount;
unsigned long Start;
unsigned long Lives;
GameStatus 	  game_status;

// Helper functions
// ----------------
void CreateFire  (unsigned long fire_in);
void ShowLives   (void);
void ShowMenu    (void);
void ShowRunning (void);
void ShowStart   (void);

void Game_Engine_Init (void)
{
	game_status = MENU;
	Refresh 		= 0;
}

// Game Engine Manager
// --------------------
void Game_Engine (void)
{
		switch (game_status)
		{
	
			case START: 		 ShowStart ();
									     break;
			
			case RUNNING:    ShowRunning ();
								       break;
			
			case SHOWLIVES:  ShowLives ();
										   break;
			
			case DEAD:       Screen_ShowDead ();
											 game_status = MENU;
							         break;
											 
			case MENU:       ShowMenu ();
										   break;
			
			default:				 game_status = RUNNING;
						
		}	
}


// HELPER FUNCTIONS IMPLEMENTATION
// -------------------------------

void ShowStart (void)
{
	Lives = 3;
	Screen_ShowLives (Lives);	
  SpriteInit (Lives);	
	game_status = RUNNING;
}

void CreateFire (unsigned long fire_in)
{
	switch (fire_in )
	{
			case FIRE1:  Fire_LED ();
								   SpriteDrawFire();
									 Sound_Shoot();
                   break;
		
			case FIRE2:  Special_FireLED ();
									 SpriteDrawMissile();
									 Sound_Explosion ();
								   break;
		
			case FIRE12: // Sprites
									 SpriteDrawFire();
									 SpriteDrawMissile();
									 // Sounds
									 Sound_Shoot();
									 Delay1ms (30);			
									 Sound_Explosion ();
									 Delay1ms (30);
									 // Leds
									 Fire_LED ();
							     Special_FireLED ();
									 break;
		
			default: 		 FireOff_LEDS ();
	}
}

void ShowRunning (void)
{
	unsigned short is_dead;
	
	SpritesMoveShip (get_X_Pos());
	CreateFire (Fire_In ());
	is_dead = SpritesEnemyMoves();
	// Sounds_Play ();
	
	if (is_dead)
	{
		Lives = Lives - 1;
		game_status = SHOWLIVES;
	}
	else
	{
		game_status = RUNNING;
	}
	
	Refresh = 1;	
}

void ShowLives (void)
{
	if (Lives)
	{
		Screen_ShowLives (Lives);
		game_status = RUNNING;
	}
	else 
		game_status = DEAD;
}

void ShowMenu (void)
{
	Screen_Menu ();
	switch (Fire_In ())
	{
		case NONE: 	 game_status = MENU;
								 break;
		case FIRE1:
		case FIRE2:
		case FIRE12: game_status = START;
							   break;
		default:		 game_status = MENU;
	}
}
